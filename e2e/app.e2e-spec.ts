import { IoasysEmpresasPage } from './app.po';

describe('ioasys-empresas App', () => {
  let page: IoasysEmpresasPage;

  beforeEach(() => {
    page = new IoasysEmpresasPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
