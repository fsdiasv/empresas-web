import { Component, OnInit } from '@angular/core';

import { Enterprise } from '../models/index';
import { EnterpriseService } from '../services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    search: string;
    results: Enterprise[] = [];
    initiliaze: boolean;
    selectedEnterprise: Enterprise;
    hideLogo: boolean = false;
    filter: string = 'filter-nome';
    p: number = 1;
    loading: boolean = false;

    constructor(private enterpriseService: EnterpriseService) { }

    ngOnInit() {
        this.initiliaze = false;
    }

    buscar() {
        this.loading = true;
        this.initiliaze = true;
        if (this.filter == "filter-nome") {
            this.enterpriseService.getEnterpriseByName(this.search)
                .subscribe(results => {
                    this.results = results;
                    this.loading = false;
                });
        } else if (this.filter == "filter-tipo") {
            this.enterpriseService.getEnterpriseByType(this.search)
                .subscribe(results => {
                    this.results = results;
                    this.loading = false;
                });
        }
    }

    listar() {
        this.loading = true;
        this.initiliaze = true;
        this.enterpriseService.getEnterprises()
            .subscribe(results => {
                this.results = results;
                this.loading = false;
            });
    }

    hiddenLogo() {
        (this.hideLogo) ? this.hideLogo = false : this.hideLogo = true;
    }
}