import { Routes, RouterModule } from '@angular/router';
 
import { LoginComponent } from './login/index';
import { HomeComponent } from './home/index';
import { EnterpriseComponent } from './enterprise/index';

import { AuthGuard } from './guards/index';
 
const appRoutes: Routes = [
    { path: 'login', component: LoginComponent },
    { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
    { path: 'enterprises/:id', component: EnterpriseComponent, canActivate: [AuthGuard] },
 
    { path: '**', redirectTo: '/home' }
];
 
export const routing = RouterModule.forRoot(appRoutes);