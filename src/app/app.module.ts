import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule } from '@angular/http';
 
// used to create fake backend
import { MockBackend, MockConnection } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';
 
import { AppComponent }  from './app.component';
import { routing }        from './app.routing';
 
import { AuthGuard } from './guards/index';
import { AuthenticationService, EnterpriseService } from './services/index';
import { LoginComponent } from './login/index';
import { HomeComponent } from './home/index';
import { EnterpriseComponent } from './enterprise/index';
import { MaterializeModule } from "angular2-materialize";
import { NgxPaginationModule } from 'ngx-pagination'; 

 
@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterializeModule,
        NgxPaginationModule,
        routing
    ],
    declarations: [
        AppComponent,
        LoginComponent,
        HomeComponent,
        EnterpriseComponent
    ],
    providers: [
        AuthGuard,
        AuthenticationService,
        EnterpriseService
    ],
    bootstrap: [AppComponent]
})
 
export class AppModule { }