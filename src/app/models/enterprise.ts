export class Enterprise {
    id: number;
    enterprise_name: string;
    photo: string;
    description: string;
    city: string;
    country: string;
}