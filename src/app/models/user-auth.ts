export class UserAuth {
    uid: string;
    token: string;
    client: string;
}