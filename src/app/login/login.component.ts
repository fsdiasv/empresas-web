import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../services/index';

@Component({
    moduleId: module.id,
    styleUrls: ['./login.component.css'],
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading: boolean = false;
    error: boolean = false;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }

    ngOnInit() {
        this.authenticationService.logout();
    }

    login() {
        this.loading = true;

        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
            result => {
                if (result) {
                    this.router.navigate(['/home']);
                }
            },
            error => {
                this.error = true;
                this.loading = false;
            }
            );
    }
}