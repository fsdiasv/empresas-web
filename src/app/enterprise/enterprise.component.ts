import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

import { Enterprise } from '../models/index';
import { EnterpriseService } from '../services/index';

import 'rxjs/add/operator/switchMap';

@Component({
    moduleId: module.id,
    templateUrl: 'enterprise.component.html',
    styleUrls: ['./enterprise.component.css']
})

export class EnterpriseComponent implements OnInit {

    @Input() enterprise: Enterprise;

    error: boolean = false;

    constructor(private enterpriseService: EnterpriseService, private route: ActivatedRoute, private _location: Location) { }

    ngOnInit() {
        this.route.paramMap
            .switchMap((params: ParamMap) => this.enterpriseService.getEnterpriseById(+params.get('id')))
            .subscribe(enterprise => this.enterprise = enterprise,
                error => {
                    this.error = true;
                });
    }

    onBack() {
        this._location.back();
    }

}