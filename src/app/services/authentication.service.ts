import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { UserAuth } from '../models/user-auth';

@Injectable()
export class AuthenticationService {
    private loginUrl: string = 'http://54.94.179.135:8090/api/v1/users/auth/sign_in';

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    headers() {
        let headersParams = { 'Content-Type': 'application/json' };
        if (localStorage['token']) {
            headersParams['Authorization'] = localStorage['token'];
        }
        let headers = new Headers(headersParams);
        let options = new RequestOptions({ headers: headers });
        return options;
    }

    login(username: string, password: string): Observable<boolean> {
        let params = JSON.stringify({ email: username, password: password });

        return this.http.post(this.loginUrl, params, this.headers())
            .map((response: Response) => {

                if (response.status == 200) {
                    let headersJson = response.headers.toJSON();

                    let userAuth = new UserAuth();
                    userAuth.token = headersJson['access-token'];
                    userAuth.uid = headersJson['uid'];
                    userAuth.client = headersJson['client'];

                    localStorage.setItem('currentUser', JSON.stringify({ headersJson }));

                    return true;
                }
            })
            .catch((error: any) => {
                return Observable.throw(new Error(error.status));
            });
    }

    logout(): void {
        localStorage.removeItem('currentUser');
    }
}