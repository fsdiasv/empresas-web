import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from '../services/index';
import { Enterprise, ENT_TYPES } from '../models/index';
import { UserAuth } from '../models/user-auth';


@Injectable()
export class EnterpriseService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService) {
    }

    headers(): Headers {
        let currentUser = JSON.parse(localStorage.getItem("currentUser"));

        let headers = new Headers();

        headers.append('Content-Type', 'application/json; charset=utf-8');
        headers.append("access-token", currentUser.headersJson['access-token']);
        headers.append("uid", currentUser.headersJson['uid']);
        headers.append("client", currentUser.headersJson['client']);

        return headers;
    }

    getEnterprises(): Observable<Enterprise[]> {
        let options = new RequestOptions({ headers: this.headers() });

        return this.http.get('http://54.94.179.135:8090/api/v1/enterprises', options)
            .map((response: Response) => <Enterprise[]>response.json().enterprises);
    }

    getEnterpriseById(id: number): Observable<Enterprise> {
        let options = new RequestOptions({
            headers: this.headers()
        });

        return this.http.get((`http://54.94.179.135:8090/api/v1/enterprises/${id}`), options)
            .map(response => response.json().enterprise);
    }

    getEnterpriseByName(name: string): Observable<Enterprise[]> {
        let options = new RequestOptions({
            headers: this.headers(),
            params: {
                'name': name
            }
        });

        return this.http.get('http://54.94.179.135:8090/api/v1/enterprises', options)
            .map((response: Response) => <Enterprise[]>response.json().enterprises);
    }

    getEnterpriseByType(type: string): Observable<Enterprise[]> {
        let options = new RequestOptions({
            headers: this.headers(),
            params: {
                'enterprise_types': this.helperEnterpriseType(type)
            }
        });

        return this.http.get('http://54.94.179.135:8090/api/v1/enterprises', options)
            .map((response: Response) => <Enterprise[]>response.json().enterprises);
    }

    helperEnterpriseType(type: string): number {
        return ENT_TYPES.indexOf(type.toLowerCase()) + 1;
    }
}